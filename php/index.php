<html>
	<head>
		<title>Hello World</title>
	</head>
	<body>
		<?php
			echo "Hello, World!";
		?>
	</body>
</html>
<!-- ./php/pdo.php -->
<?php
try {
	$servername = "db";
	$username = "root";
	$password = "my_secret_pw_shh";
	$dbname = "example_db";
	$port = "3306";
	# Conexión a MySQL
	$db = new PDO("mysql:host=$servername;port=$port;dbname=$dbname",$username,$password);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$respuesta = $db->query('SELECT * from example_table');
	//var_dump($db->query('SELECT * from example_table'));
	//Creamos un array donde almacenaremos la data obtenida
	$usuarios = [];
	//Recorremos la data obtenida
	foreach($respuesta as $res){
		//Llenamos la data en el array
		$usuarios[]=$res;
	}
	//Hacemos una impresion del array en formato JSON.
	echo json_encode($usuarios);
}
catch(PDOException $e) {
	echo $e->getMessage();
}